package ru.t1.sochilenkov.tm.service;

import ru.t1.sochilenkov.tm.api.repository.ITaskRepository;
import ru.t1.sochilenkov.tm.api.service.ITaskService;
import ru.t1.sochilenkov.tm.model.Task;

import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        if (description != null && !description.isEmpty())
            task.setDescription(description);
        return add(task);
    }

    @Override
    public Task add(final Task project) {
        if (project == null) return null;
        return taskRepository.add(project);
    }

    @Override
    public Task findOneById(String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task remove(Task task) {
        if (task == null) return null;
        return taskRepository.remove(task);
    }

    @Override
    public Task removeById(String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findOneById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(Integer index, String name, String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = findOneByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

}
