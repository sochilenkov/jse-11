package ru.t1.sochilenkov.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}
