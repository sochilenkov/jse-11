package ru.t1.sochilenkov.tm.model;

public final class Command {

    public Command() {
    }

    public Command(
            final String name,
            final String argument,
            final String description
    ) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    private String name;

    private String argument;

    private String description;

    public String getName() {
        return name;
    }

    public String getArgument() {
        return argument;
    }

    public String getDescription() {
        return description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        final boolean hasName = name != null && !name.isEmpty();
        final boolean hasArgument = argument != null && !argument.isEmpty();
        final boolean hasDescription = description != null && !description.isEmpty();
        if (hasName) result += name;
        if (hasArgument) result += hasName ? ", [" + argument + "]" : "[" + argument + "]";
        if (hasDescription) result += " -- " + description;
        return result;
    }

}
