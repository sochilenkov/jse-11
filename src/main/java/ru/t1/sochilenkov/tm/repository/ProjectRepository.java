package ru.t1.sochilenkov.tm.repository;

import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.api.repository.IProjectRepository;


import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public Project add(Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findOneById(String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(Integer index) {
        return projects.get(index);
    }

    @Override
    public Project remove(Project project) {
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeById(String id) {
        final Project project = findOneById(id);
        if (project == null) return null;
        return remove(project);
    }

    @Override
    public Project removeByIndex(Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        return remove(project);
    }
}
