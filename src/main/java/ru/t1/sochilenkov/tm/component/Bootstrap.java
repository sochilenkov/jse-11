package ru.t1.sochilenkov.tm.component;

import ru.t1.sochilenkov.tm.api.controller.ICommandController;
import ru.t1.sochilenkov.tm.api.controller.IProjectController;
import ru.t1.sochilenkov.tm.api.controller.ITaskController;
import ru.t1.sochilenkov.tm.api.repository.ICommandRepository;
import ru.t1.sochilenkov.tm.api.repository.IProjectRepository;
import ru.t1.sochilenkov.tm.api.repository.ITaskRepository;
import ru.t1.sochilenkov.tm.api.service.ICommandService;
import ru.t1.sochilenkov.tm.api.service.IProjectService;
import ru.t1.sochilenkov.tm.api.service.ITaskService;
import ru.t1.sochilenkov.tm.constant.ArgumentConstant;
import ru.t1.sochilenkov.tm.constant.CommandConstant;
import ru.t1.sochilenkov.tm.controller.CommandController;
import ru.t1.sochilenkov.tm.controller.ProjectController;
import ru.t1.sochilenkov.tm.controller.TaskController;
import ru.t1.sochilenkov.tm.repository.CommandRepository;
import ru.t1.sochilenkov.tm.repository.ProjectRepository;
import ru.t1.sochilenkov.tm.repository.TaskRepository;
import ru.t1.sochilenkov.tm.service.CommandService;
import ru.t1.sochilenkov.tm.service.ProjectService;
import ru.t1.sochilenkov.tm.service.TaskService;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private void processCommands() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private static void exit() {
        System.exit(0);
    }

    private void processCommand(final String command) {
        switch (command) {
            case CommandConstant.VERSION:
                commandController.showVersion();
                break;
            case CommandConstant.HELP:
                commandController.showHelp();
                break;
            case CommandConstant.ABOUT:
                commandController.showAbout();
                break;
            case CommandConstant.INFO:
                commandController.showSystemInfo();
                break;
            case CommandConstant.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConstant.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConstant.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConstant.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConstant.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConstant.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConstant.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConstant.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConstant.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConstant.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConstant.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CommandConstant.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConstant.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConstant.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConstant.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConstant.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConstant.EXIT:
                exit();
                break;
            default:
                commandController.showErrorCommand();
        }
    }

    private void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConstant.INFO:
                commandController.showSystemInfo();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    public void run(final String... args) {
        processArguments(args);
        processCommands();
    }

}
